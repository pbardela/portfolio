import UserProfile from '../components/UserProfile';
import Experiences from '../components/Experiences';
import Certificates from '../components/Certificates';

const Home = () => {
  return (
    <div id='home'>
      <UserProfile />
      <Experiences />
      <Certificates />
    </div>
  );
};

export default Home;
