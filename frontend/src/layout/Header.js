import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import Toolbar from '@mui/material/Toolbar';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import { useTheme } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/Menu';

import CustomButton from '../components/CustomButton';

// Font Awesome Icons
import logopab from '../assets/logo/logo.jpg';

const Header = ({ onSidebarOpen }) => {
  const theme = useTheme();
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 38,
  });

  return (
    <>
      <AppBar
        position='sticky'
        elevation={trigger ? 1 : 0}
        sx={{
          top: 0,
          border: 0,
          backgroundColor: trigger
            ? theme.palette.background.default
            : 'transparent',
        }}
      >
        <Toolbar sx={{ height:45}}>
          <IconButton
            onClick={() => onSidebarOpen()}
            aria-label='Menu'
            sx={{
              color: theme.palette.primary.main,
              display: { xs: 'block', md: 'none' },
            }}
          >
            <MenuIcon fontSize='medium' />
          </IconButton>
          <Link href='/' style={{ textDecoration: 'none' }}>
            <IconButton size='large' disabled>
              <IconButton
                href='#'
                onClick={() => {
                  console.log("click");
                }}
              >
                <img src={logopab} width="45" height="45"  alt="My logo"/>
              </IconButton>

            </IconButton>
          </Link>
          <Box sx={{ flexGrow: 1 }} />
          <Box
            sx={{
              alignItems: 'center',
              display: { md: 'flex', xs: 'none' },
            }}
          >
            <CustomButton href='/' text='Home' />
            {/* <li><a href={Experiences}>Experiences</a></li> */}
            <CustomButton href='/Experiences' text='Experiences' />
            {/* <CustomButton href='#technologies' text='Technologies' />
            <CustomButton href='#testimonials' text='Testimonials' />
            <CustomButton href='#contact' text='Contact' /> */}
            <CustomButton href='/Interests' text='About Me' />
          </Box>
        </Toolbar>
      </AppBar>
    </>
  );
};

Header.propTypes = {
  onSidebarOpen: PropTypes.func,
};

export default Header;
