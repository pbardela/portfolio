
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import logogitlab from '../assets/logo/gitlab.svg';
import logolinkedin from '../assets/logo/linkedin-icon.svg';
import logogithub from '../assets/logo/github.svg';

const Footer = ({ onSidebarOpen }) => {
  const theme = useTheme();

  return (
    <>
      <Box
        backgroundColor={theme.palette.background.default}
        paddingTop='1px'
        paddingBottom='1px'
        bottom='0'
        left='0'
        width='100%'
      >
        <Divider />
        <Box
          backgroundColor={theme.palette.background.default}
          position='center'
          padding={theme.spacing(0.25)}
        >
            <Box sx={{ display: { lg: 'flex', md: 'none', xs: 'none',  justifyContent: 'center' } }}>
              <IconButton
                href='https://www.linkedin.com/in/paul-alexandre-bardela/?locale=en_US'
              >
                <img src={logolinkedin}  width="30" height="30" alt="LinkedIn"/>
              </IconButton>
              <IconButton
                href='https://gitlab.com/pbardela'
              >
                <img src={logogitlab}  width="30" height="30" alt="Gitlab"/>
              </IconButton>
              <IconButton
                href='https://github.com/pbardel'
              >
                <img src={logogithub}  width="30" height="30" alt="GitHub"/>
              </IconButton>
            </Box>
        </Box>
      </Box>
    </>
  );
};

export default Footer;
