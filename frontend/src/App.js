import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { HelmetProvider, Helmet } from 'react-helmet-async';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import 'aos/dist/aos.css';

import theme from './theme/theme';
import Layout from './layout/Layout';
import Home from './pages/Home';
import Interests from './components/InterestsPage';
import ExperiencesPage from './components/ExperiencesPage';
import ExperiencesDetail from './components/ExperiencesDetail';
import Certificates from './components/Certificates';
import Contact from './components/Contact';
import GitUpdate from './components/GitUpdate';

const App = () => {
  return (
    <HelmetProvider>
      <Helmet
        titleTemplate="%s | Paul, Software Engineer"
        defaultTitle="Paul, Software Engineer"
      />
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <Layout>
            <Routes>
              <Route exact path='/' element={<Home />} />
              <Route exact path='/Experiences' element={<ExperiencesPage />} />
              <Route exact path='/Experiences/:slug' element={<ExperiencesDetail />} />
              <Route exact path='/Certificates' element={<Certificates />} />
              <Route exact path='/Interests' element={<Interests />} />
              <Route exact path='/Contact' element={<Contact />} />
              <Route exact path='/GitUpdate' element={<GitUpdate />} />
            </Routes>
          </Layout>
        </BrowserRouter>
      </ThemeProvider>
    </HelmetProvider>
  );
};

export default App;
