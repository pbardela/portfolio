import { useState } from 'react';
import axios from 'axios';
import Box from '@mui/material/Box';

function Contact() {
  const [formData, setFormData] = useState({
    name: "",
    subject: "", 
    phone: "",
    email: "",
    message: "",
  });
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await axios.post(
        "https://pbardel.pythonanywhere.com/api/contact/",
        formData
      );
      alert("Form Submitted");
      setFormData({ name: "", email: "", message: "" });
    } catch (error) {
      console.error("Error submitting form:", error);
      alert("Error submitting form. Please try again later.");
    }
  };
  return (
    <div id='contact'>
      <Box position='relative' marginBottom={15}>
        <Box
          maxWidth={{ sm: 720, md: 1236 }}
          width={1}
          margin='0 auto'
          paddingX={2}
          paddingY={{ xs: 4, sm: 6, md: 8 }}
          paddingBottom={10}
        >
          <form onSubmit={handleSubmit}>
            <div className="mb-2">
              <input
                type="text"
                id="name"
                name="name"
                value={formData.name}
                onChange={handleChange}
                placeholder="Name"
                className="w-full p-2 border border-gray-300 rounded"
                required
              />
            </div>
            <div className="mb-2">
              <input
                type="email"
                id="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                placeholder="Email"
                className="w-full p-2 border border-gray-300 rounded"
                required
              />
            </div>

            <div className="mb-2">
              <textarea
                id="message"
                name="message"
                value={formData.message}
                onChange={handleChange}
                placeholder="Message"
                rows="3" 
                className="w-full p-2 border border-gray-300 rounded"
                required
              ></textarea>
            </div>

            <button
              type="submit"
              className="bg-green-400 text-white p-2 rounded-2xl"
              style={{ width: "355px" }}
            >
              SUBMIT
            </button>
          </form>

        </Box>
      </Box>
    </div>
  );
};

export default Contact;
