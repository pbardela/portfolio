


import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';
import AOS from 'aos';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import "./style.css"
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Box from '@mui/material/Box';
import Interests from './Interests';


const InterestProfile = () => {
  const theme = useTheme();
  const [InterestProfile, setInterestProfile] = useState([]);

  const fetchInterestProfile = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setInterestProfile(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchInterestProfile();
  }, []);

  useEffect(() => {
    AOS.init({
      once: true,
      delay: 50,
      duration: 600,
      easing: 'ease-in-out',
    });
  }, []);

  return (
  <section>
    <Box
      maxWidth={{ sm: 720, md: 1236 }}
      width={1}
      margin='0 auto'
      paddingX={2}
      paddingY={4}
    >
      {InterestProfile.slice(0, 1).map((user, i) => (
        <Grid container  spacing={2} display='grid' gridTemplateColumns='70% auto'>
          <Grid item sx={{ order: { xs: 2, md: 1 } }}>
              <Box marginBottom={2}>
                <Typography
                  color={theme.palette.text.primary}
                  variant='h1'
                  fontWeight={700}
                  marginTop={3}
                  align='Left'
                >
                  Personal Interests {' '}
                </Typography>
              </Box>
              <Box marginBottom={3}>
                <Typography
                  variant='h4'
                  component='p'
                  color={theme.palette.text.primary}
                  align='justify'
                >
                  {user.bio_personal}
                </Typography>
              </Box>
          </Grid>
          <Grid
            item
            container
            alignItems='center'
            justifyContent='center'
            verticalAlign='middle'
            sx={{ order: { xs: 1, md: 2 } }}
          >
            <Box
              sx={{
                '& img': {
                  objectFit: 'cover',
                },
                '& .lazy-load-image-loaded': {
                  height: 1,
                  width: 1,
                },
                
              }}
            >
              <Box
                component={LazyLoadImage}
                src={user.avatar_personal}
                alt='Background Image'
                effect='blur'
                height={240}
                maxHeight={{ xs: 300, md: 1 }}
                width={240}
                maxWidth={1}
                borderRadius={50}
              />
            </Box>
          </Grid>
        </Grid>
      ))}
      <Interests />
    </Box>
  </section>
  );
};

export default InterestProfile;
