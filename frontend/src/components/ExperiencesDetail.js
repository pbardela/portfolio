


import React from "react";
import { useParams } from "react-router-dom"
import { useState, useEffect } from 'react';
import AOS from 'aos';
import axios from 'axios';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import "./style.css"

const ExperiencesDetail = () => {
  const theme = useTheme();
  const { slug } = useParams();

  const [experiencesDetail, setExperiencesDetail] = useState([]);


  useEffect(() => {
    const fetchExperiencesDetail = () => {
      axios
        .get(`https://pbardel.pythonanywhere.com/api/Experiences/${slug}/`, {
          headers: {
            Accept: 'application/json',
          },
        })
        .then((response) => {
          setExperiencesDetail([response.data]);
        })
        .catch((error) => console.log(error));
    };
    fetchExperiencesDetail();
  }, [slug]);

  useEffect(() => {
    AOS.init({
      once: true,
      delay: 50,
      duration: 600,
      easing: 'ease-in-out',
    });
  }, []);

  return (
  <section>    
    <Typography
      color={theme.palette.text.primary}
      variant='h2'
      fontWeight={700}
      marginLeft={1}
      marginBottom={3}
      align='left'
      >
      {experiencesDetail.name}
    </Typography>  
    <div class="container" id='experiencesDetail'>
      <div class="sectionSpaceSm lightBg">
        {experiencesDetail.map((item) => (    
          <Grid container display='grid' gridTemplateColumns='50% auto'>
            <Grid item sx={{ order: { xs: 2, md: 1 } }}>
            <div key={item.description} class="cardStyle1">    
              <Typography
                color={theme.palette.text.secondary}
                variant='h2'
                fontWeight={700}
                marginLeft={1}
                marginBottom={3}
                align='left'
              >
                {item.name}
              </Typography>               
                <Box marginLeft={1}><p>{item.description}</p></Box>    
                {item.skills.map((skill) => (                      
                  <span class="skills__name">{skill.name}</span>
                ))}
              </div>   
            </Grid>
            <Grid item sx={{ order: { xs: 1, md: 2 } }}>
              <div key={item.name} class="cardStyle1">
                <Grid container display='grid' gridTemplateColumns='50% auto'>
                  <Grid item textAlign='left' sx={{ order: { xs: 2, md: 1 } }}>
                    <span class="locationLbl">{item.location}</span> 
                  </Grid>
                  <Grid item textAlign='right' sx={{ order: { xs: 1, md: 2 } }}>
                    <span class="dateLbl">{item.date}</span>
                  </Grid>
                </Grid>                      
                <div dangerouslySetInnerHTML={{__html: item.body}}/>
              </div>   
            </Grid>
          </Grid>
        ))}
      </div>
    </div>
  </section>
  );
};

export default ExperiencesDetail;
