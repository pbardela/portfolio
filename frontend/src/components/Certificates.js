


import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import "./style.css"

const Certificates = () => {
  const theme = useTheme();

  const [certificates, setCertificates] = useState([]);

  const fetchCertificates = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/Certificates/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setCertificates(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchCertificates();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false
  };
  return (
<section>
<Box
  maxWidth={{ sm: 720, md: 1236 }}
  width={1}
  margin='0 auto'
  paddingX={2}
  paddingY={2}
>
  <div class="sectionSpaceSm lightBg">
    <div class="grid" style={{display:'flex'}}>
      <div class="col" style={{flexDirection:'column',width:'50%'}}>
      <Typography
        color={theme.palette.text.primary}
        variant='h2'
        fontWeight={500}
        marginLeft={4}
        paddingTop={2}
        align='left'
      >
      <u>Education</u>
      </Typography>  
      </div>
    </div>
    <Slider {...settings}>
      {/** Slides */}
      {certificates.map((item, i) => (
          <div key={item.name} class="cardStyle1">            
            <Typography
                color={theme.palette.text.secondary}
                variant='h3'
                fontWeight={700}
                marginLeft={1}
                marginBottom={2}
                align='left'
              >
              {item.title}
              </Typography>               
              <Typography
                  color={theme.palette.text.primary}
                  fontWeight={500}
                  marginLeft={1}
                  align='left'
                >
                <ul class="cardOptionCol" >  
                  <li><span class="dateLbl">{item.date}</span></li>
                  <li><span class="locationLbl">{item.organisation}</span></li>          
                </ul>       
                <div dangerouslySetInnerHTML={{__html: item.description}}/>  
              </Typography>            
            {item.skills.map((skill, i) => (                      
              <span class="skills__name">{skill.name}</span>
            ))}                
            <Link style={{ textDecoration: 'none' }} marginLeft={1}>
              <div style={{paddingLeft:10}} dangerouslySetInnerHTML={{__html: item.url}}/>
            </Link>
          </div>
      ))}
    </Slider>
  </div>
</Box>
</section>
);
};


export default Certificates;
