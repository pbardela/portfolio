import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';

const GitUpdate = () => {

  const [GitUpdate, setGitUpdate] = useState([]);

  const fetchGitUpdate = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/GitUpdate/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setGitUpdate(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchGitUpdate();
  }, []);


  return (
  <section>
    {GitUpdate.map(() => (      
      <h1>Update to last commit</h1>
    ))}
  </section>
  );
};

export default GitUpdate;
