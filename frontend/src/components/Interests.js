


import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import "./style.css"
import { LazyLoadImage } from 'react-lazy-load-image-component';

const Interests = () => {
  const theme = useTheme();

  const [interests, setInterests] = useState([]);

  const fetchInterests = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/About/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setInterests(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchInterests();
  }, []);

  return (
  <section>
    <div class="sectionSpaceSm lightBg">
      {interests.map((item) => (        
        <Box>          
          <Grid container display='grid' gridTemplateColumns='70% auto'>
            <Grid item sx={{ order: { xs: 1, md: 2 } }}>
              <div key={item.name} class="cardStyle1">
                <Grid container display='grid' gridTemplateColumns='50% auto'>
                  <Grid item textAlign='left' sx={{ order: { xs: 1, md: 2 } }}> 
                    <Typography
                      color={theme.palette.text.primary}
                      variant='h3'
                      fontWeight={700}
                      marginLeft={0}
                      marginBottom={3}
                      align='left'
                    >
                    {item.title} 
                    </Typography>  
                  </Grid>
                  <Grid item textAlign='right' sx={{ order: { xs: 1, md: 2 } }}> 
                      <span class="dateLbl">{item.date}</span>
                  </Grid>
                </Grid>                       
                  {item.skills.map((skill, i) => (                      
                    <span class="skills__name">{skill.name}</span>
                  ))}
                  <div dangerouslySetInnerHTML={{__html: item.body}}/>
              </div>   
            </Grid>
            <Grid
              item
              container
              alignItems='center'
              justifyContent='center'
              verticalAlign='middle'
              sx={{ order: { xs: 1, md: 2 } }}
            >
              <Box
                height={240}
                verticalAlign='middle'
                sx={{
                  '& img': {
                    objectFit: 'cover',
                  },
                  '& .lazy-load-image-loaded': {
                    height: 1,
                    width: 1,
                  },
                  
                }}
              >
                <Box
                  component={LazyLoadImage}
                  verticalAlign='middle'
                  src={item.image}
                  height={240}
                  maxHeight={{ xs: 240, md: 1 }}
                  width={240}
                  maxWidth={1}
                />
              </Box>
            </Grid>
          </Grid>
          <Divider sx={{ backgroundColor: "#08429857", height: "3px", margin: "16px 16px" }} />
        </Box>
      ))}
    </div>
  </section>
  );
};

export default Interests;
