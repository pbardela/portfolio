import { useState, useEffect } from 'react';
import axios from 'axios';
import AOS from 'aos';
import useMediaQuery from '@mui/material/useMediaQuery';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import ContactMail from '@mui/icons-material/ContactMail';
import ManageSearchIcon from '@mui/icons-material/ManageSearchOutlined';
import Button from '@mui/material/Button';
import React from "react";
import Slider from "react-slick";

const UserProfile = () => {
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up('md'), {
    defaultMatches: true,
  });

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false
  };
  const [UserProfile, setUserProfile] = useState([]);

  const fetchUserProfile = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setUserProfile(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchUserProfile();
  }, []);

  useEffect(() => {
    AOS.init({
      once: true,
      delay: 50,
      duration: 600,
      easing: 'ease-in-out',
    });
  }, []);

  return (
    <div id='home'>
      {UserProfile.slice(0, 1).map((item, i) => (
        <Box
          maxWidth={{ sm: 720, md: 1236 }}
          width={1}
          margin='0 auto'
          paddingX={2}
          paddingY={2}
        >
          <Grid container key={i} spacing={2} display='grid' gridTemplateColumns='75% auto'>
            <Grid item sx={{ order: { xs: 2, md: 1 } }}>
                <Box marginBottom={2}>
                  <Typography
                    color={theme.palette.text.primary}
                    variant='h1'
                    fontWeight={700}
                    marginTop={0}
                    align='left'
                  >
                    Hi, I'm Paul {' '}
                  </Typography>
                </Box>
                <Box marginBottom={3}>
                  <Typography
                    variant='h4'
                    component='p'
                    color={theme.palette.text.primary}
                    align='justify'
                  >
                    {item.bio}
                  </Typography>
                </Box>
                <Box
                  display='flex'
                  flexDirection={{ xs: 'column', sm: 'row' }}
                  alignItems={{ xs: 'stretched', sm: 'flex-start' }}
                  justifyContent='center'
                  marginTop={4}
                >
                  <Button
                    component='a'
                    variant='contained'
                    color='primary'
                    size='large'
                    href={item.cv}
                    endIcon={<ManageSearchIcon />}
                    fullWidth={isMd ? false : true}
                    disableElevation={true}
                    sx={{
                      marginRight: '15px',
                      border: `2px solid ${theme.palette.primary.main}`,
                      '&:hover': {
                        backgroundColor: 'transparent',
                        color: theme.palette.primary.main,
                        border: `2px solid ${theme.palette.primary.main}`,
                      },
                    }}
                  >
                    Download Resume
                  </Button>
                  <Box
                    marginTop={{ xs: 2, sm: 0 }}
                    marginLeft={{ sm: 1 }}
                    width={{ xs: '100%', md: 'auto' }}
                  >
                    <Button
                      component='a'
                      variant='outlined'
                      color='primary'
                      size='large'
                      href='/Contact'
                      endIcon={<ContactMail />}
                      fullWidth={isMd ? false : true}
                      disableElevation={true}
                      sx={{
                        border: `2px solid ${theme.palette.primary.main}`,
                        '&:hover': {
                          backgroundColor: theme.palette.primary.main,
                          color: theme.palette.common.white,
                          border: `2px solid ${theme.palette.primary.main}`,
                        },
                      }}
                    >
                      Contact
                    </Button>
                  </Box>
                </Box>
            </Grid>
            <Grid
              item
              container
              alignItems='center'
              justifyContent='center'
              verticalAlign='middle'
              sx={{ order: { xs: 1, md: 2 } }}
            >
              <Box
                sx={{
                  '& img': {
                    objectFit: 'cover',
                  },
                  '& .lazy-load-image-loaded': {
                    height: 1,
                    width: 1,
                  },
                  
                }}
              >
                <Box
                  component={LazyLoadImage}
                  src={item.avatar}
                  alt='Background Image'
                  effect='blur'
                  height={{ xs: 'auto', md: 1 }}
                  maxHeight={{ xs: 300, md: 1 }}
                  width={1}
                  maxWidth={1}
                  borderRadius={50}
                />
              </Box>
            </Grid>
          </Grid>
        <div class="sectionSpaceSm lightBg" >
            <Typography
              color={theme.palette.text.primary}
              variant='h2'
              fontWeight={500}
              marginLeft={4}
              paddingTop={2}
              align='left'
            >
            <u>Skills</u>
            </Typography>
          <Slider {...settings}>
            {/** Slides */}
            {item.categories.map((category) => (
                <div key={category.name} class="cardStyle1">
                  <Typography
                    color={theme.palette.text.secondary}
                    variant='h3'
                    fontWeight={700}
                    marginLeft={1}
                    marginBottom={2}
                    align='left'
                  >
                  {category.name}
                  </Typography>             
                  {item.skills.filter((skill) => skill.category.name === category.name).map((skill) => (                      
                    <span class="skills__name">{skill.name}</span>
                  ))}
                </div>
            ))}
          </Slider>
        </div>
      </Box>
    ))}
    </div>
  );
};

export default UserProfile;
