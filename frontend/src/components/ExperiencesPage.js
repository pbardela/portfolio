


import React from "react";
import { useState, useEffect } from 'react';
import axios from 'axios';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import "./style.css"

const ExperiencesPage = () => {
  const theme = useTheme();

  const [experiencesPage, setExperiencesPage] = useState([]);

  const fetchExperiencesPage = () => {
    axios
      .get('https://pbardel.pythonanywhere.com/api/Experiences/', {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => {
        setExperiencesPage(response.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchExperiencesPage();
  }, []);
  return (
  <section>    
    <Typography
      color={theme.palette.text.primary}
      variant='h2'
      fontWeight={700}
      marginLeft={1}
      marginBottom={3}
      align='left'
      >
      Experiences
    </Typography>  
    <div class="container" id='experiencesPage'>
      <div class="sectionSpaceSm lightBg">
        {experiencesPage.map((item, i) => (
          <Box>               
            <Grid container key={i} display='grid' gridTemplateColumns='50% auto'>
              <Grid item sx={{ order: { xs: 2, md: 1 } }}>
              <div key={item.name} class="cardStyle1">    
                <Typography
                  color={theme.palette.text.secondary}
                  variant='h3'
                  fontWeight={700}
                  marginLeft={1}
                  marginBottom={3}
                  align='left'
                >
                  {item.name}
                </Typography>
                  {item.skills.map((skill, i) => (                      
                    <span class="skills__name">{skill.name}</span>
                  ))}
                </div>   
              </Grid>
              <Grid item sx={{ order: { xs: 1, md: 2 } }}>
                <div key={item.name} class="cardStyle1">
                  <Grid container key={i} display='grid' gridTemplateColumns='50% auto'>
                    <Grid item textAlign='left' sx={{ order: { xs: 2, md: 1 } }}>
                      <span class="locationLbl">{item.location}</span> 
                    </Grid>
                    <Grid item textAlign='right' sx={{ order: { xs: 1, md: 2 } }}>
                      <span class="dateLbl">{item.date}</span>
                    </Grid>
                  </Grid>
                  <div dangerouslySetInnerHTML={{__html: item.body}}/>
                </div>
            </Grid>
          </Grid>
          <Divider sx={{ backgroundColor: "#08429857", height: "3px", margin: "0 16px" }} />
        </Box>
      ))}
      </div>
    </div>
  </section>
  );
};

export default ExperiencesPage;
