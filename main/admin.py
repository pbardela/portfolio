from django.contrib import admin
from . models import (
    UserProfile,
    ContactProfile,
    Testimonial,
    Media,
    Portfolio,
    Experience,
    Certificate,
    Skill,
    Category,
    Interest
    )


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('id', 'user')

@admin.register(ContactProfile)
class ContactAdmin(admin.ModelAdmin):
	list_display = ('id', 'timestamp', 'name',)

@admin.register(Interest)
class InterestAdmin(admin.ModelAdmin):
    list_display = ('id','title','order','is_active')
    ordering = ('-order',)

@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('id','name','is_active')

@admin.register(Media)
class MediaAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    list_display = ('id','name','is_active')
    readonly_fields = ('slug',)

@admin.register(Experience)
class ExperienceAdmin(admin.ModelAdmin):
    list_display = ('id','name','order','is_active')
    readonly_fields = ('slug',)
    ordering = ('-order',)

@admin.register(Certificate)
class CertificateAdmin(admin.ModelAdmin):
    list_display = ('id','title','order')
    ordering = ('-order',)

@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('id','name','category','order')
    ordering = ('-order',)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','name','order')
    ordering = ('-order',)