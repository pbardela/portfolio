from django.shortcuts import render
# from django.contrib import messages
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework import permissions
from .models import (
		UserProfile,
		Experience,
		Portfolio,
		Certificate,
		ContactProfile,
		Interest
	)
from .serializers import (
		UserProfileSerializer,
		ExperienceSerializer,
		CertificateSerializer,
		PortfolioSerializer,
        ContactProfileSerializer,
		InterestSerializer
	)
from django.views import generic
from . forms import ContactForm
import git,os
from django.views.decorators.csrf import csrf_exempt

class UserProfileView(ListAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = UserProfile.objects.all()
	serializer_class = UserProfileSerializer
	pagination_class = None
	# template_name = "main/index.html"

	# def get_context_data(self, **kwargs):
	# 	context = super().get_context_data(**kwargs)
		
	# 	testimonials = Testimonial.objects.filter(is_active=True)
	# 	certificates = Certificate.objects.filter(is_active=True).order_by('title').order_by('-order')
	# 	Experiences = Experience.objects.filter(is_active=True).order_by('name').order_by('-order')
	# 	portfolio = Portfolio.objects.filter(is_active=True)
	# 	categories = Category.objects.filter(is_active=True).order_by('name').order_by('-order')
		
	# 	context["testimonials"] = testimonials
	# 	context["certificates"] = certificates
	# 	context["Experiences"] = Experiences
	# 	context["portfolio"] = portfolio
	# 	context["categories"] = categories
	# 	return context


class InterestView(ListAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = Interest.objects.all().filter(is_active=True).order_by('-order')
	serializer_class = InterestSerializer
	pagination_class = None

class PortfolioView(ListAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = Portfolio.objects.all()
	serializer_class = PortfolioSerializer
	pagination_class = None
	# model = Portfolio
	# template_name = "main/portfolio.html"
	# paginate_by = 10

	# def get_queryset(self):
	# 	return super().get_queryset().filter(is_active=True)


# class PortfolioDetailView(generic.DetailView):
	# model = Portfolio
	# template_name = "main/portfolio-detail.html"

class ExperienceView(ListAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = Experience.objects.all().filter(is_active=True).order_by('-order')
	serializer_class = ExperienceSerializer
	pagination_class = None
	# model = Experience
	# template_name = "main/Experience.html"
	# paginate_by = 10
	
	# def get_queryset(self):
	# 	return super().get_queryset().filter(is_active=True).order_by('-order')

class ExperienceDetailView(RetrieveAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = Experience.objects.all()
	serializer_class = ExperienceSerializer
	pagination_class = None
	lookup_field = 'slug'

# class ExperienceDetailView(generic.DetailView):
	# model = Experience
	# template_name = "main/Experience-detail.html"

class CertificateView(ListAPIView):
	permission_classes = (permissions.AllowAny, )
	queryset = Certificate.objects.all()
	serializer_class = CertificateSerializer
	pagination_class = None

class ContactProfileView(APIView):
      
    def post(self, request, format=None):
        serializer = ContactProfileSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({'message': 'Form submitted successfully!'}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer)

repo_name = "portfolio"
@csrf_exempt
def GitUpdate(request):
    if not(repo_name in os.getcwd()):
        os.chdir(repo_name)
    repo = git.Repo('./portfolio', search_parent_directories=True)
    for remote in repo.remotes:
        remote.fetch()
    head = sorted(repo.remotes.origin.refs, key=lambda t: t.commit.committed_datetime)[-1]
    head.checkout()
    return render(request, 'main/GitUpdate.html')
