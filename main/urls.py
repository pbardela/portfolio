from django.urls import path, re_path
from . import views
from django.views.generic import TemplateView

app_name = "main"
urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html")),
	path('api/', views.UserProfileView.as_view(), name="home"),
    path('api/GitUpdate/', views.GitUpdate, name='GitUpdate'),
	path('api/About/', views.InterestView.as_view(), name="about"),
	path('api/contact/', views.ContactProfileView.as_view(), name="contact"),
	path('api/portfolio/', views.PortfolioView.as_view(), name="portfolios"),
	path('api/portfolio/<slug:slug>/', views.PortfolioView.as_view(), name="portfolio"),
	path('api/Experiences/', views.ExperienceView.as_view(), name="Experiences"),
	path('api/Experiences/<slug:slug>/', views.ExperienceDetailView.as_view(), name="Experiences"),
	path('api/Certificates/', views.CertificateView.as_view(), name="Certificates"),
    # re_path(r'.*', TemplateView.as_view(template_name="index.html")),
	]
