from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from ckeditor.fields import RichTextField

class Category(models.Model):
    name = models.CharField(max_length=20)
    order = models.IntegerField(default=1)

    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name

class Skill(models.Model):
    category = models.ForeignKey(Category,
                                on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    is_active = models.BooleanField(default=True)
    order = models.IntegerField(default=1)

    class Meta:
        verbose_name_plural = 'Skills'
        verbose_name = 'SKill'
        ordering = ["-order"]

    def __str__(self):
        return self.name

class UserProfile(models.Model):

    class Meta:
        verbose_name_plural = 'User Profiles'
        verbose_name = 'User Profile'
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(blank=True, null=True, upload_to="avatar")
    title = models.CharField(max_length=200, blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    bio_personal = models.TextField(blank=True, null=True)
    avatar_personal = models.ImageField(blank=True, null=True, upload_to="avatar")
    skills = models.ManyToManyField(Skill, blank=True)
    categories = models.ManyToManyField(Category, blank=True)
    cv = models.FileField(blank=True, null=True, upload_to="cv")
    linkedin_url = models.CharField(max_length=200, blank=True, null=True)
    github_url = models.CharField(max_length=200, blank=True, null=True)
    gitlab_url = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'


class Interest(models.Model):

    class Meta:
        verbose_name_plural = 'Interests'
        verbose_name = 'Interest'
    
    title = models.CharField(verbose_name="Title",max_length=100)
    body = RichTextField(blank=True, null=True)
    skills = models.ManyToManyField(Skill, blank=True)
    date = models.CharField(max_length=200, blank=True, null=True)
    url = RichTextField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    order = models.IntegerField(default=1)
    image = models.ImageField(blank=True, null=True, upload_to="avatar")

    def __str__(self):
        return f'{self.title}'


class ContactProfile(models.Model):
    
    class Meta:
        verbose_name_plural = 'Contact Profiles'
        verbose_name = 'Contact Profile'
        ordering = ["timestamp"]
    timestamp = models.DateTimeField(auto_now_add=True)
    name = models.CharField(verbose_name="Name",max_length=100)
    email = models.EmailField(verbose_name="Email")
    message = models.TextField(verbose_name="Message")

    def __str__(self):
        return f'{self.name}'


class Testimonial(models.Model):

    class Meta:
        verbose_name_plural = 'Testimonials'
        verbose_name = 'Testimonial'
        ordering = ["name"]

    thumbnail = models.ImageField(blank=True, null=True, upload_to="testimonials")
    name = models.CharField(max_length=200, blank=True, null=True)
    role = models.CharField(max_length=200, blank=True, null=True)
    quote = models.CharField(max_length=500, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Media(models.Model):

    class Meta:
        verbose_name_plural = 'Media Files'
        verbose_name = 'Media'
        ordering = ["name"]
	
    image = models.ImageField(blank=True, null=True, upload_to="media")
    url = models.URLField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    is_image = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.url:
            self.is_image = False
        super(Media, self).save(*args, **kwargs)
    def __str__(self):
        return self.name

class Portfolio(models.Model):

    class Meta:
        verbose_name_plural = 'Portfolio Profiles'
        verbose_name = 'Portfolio'
        ordering = ["name"]
    date = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    body = RichTextField(blank=True, null=True)
    image = models.ImageField(blank=True, null=True, upload_to="portfolio")
    url = RichTextField(blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Portfolio, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f"/portfolio/{self.slug}"


class Experience(models.Model):

    class Meta:
        verbose_name_plural = 'Experience Profiles'
        verbose_name = 'Experience'
        ordering = ["timestamp"]

    timestamp = models.DateTimeField(auto_now_add=True)
    date = models.CharField(max_length=200, blank=True, null=True)
    location = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)
    body = RichTextField(blank=True, null=True)
    slug = models.SlugField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    skills = models.ManyToManyField(Skill, blank=True)
    url = RichTextField(blank=True, null=True)
    order = models.IntegerField(default=1)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Experience, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f"/Experience/{self.slug}"


class Certificate(models.Model):

    class Meta:
        verbose_name_plural = 'Certificates'
        verbose_name = 'Certificate'

    date = models.CharField(max_length=50, blank=True, null=True)
    organisation = models.CharField(max_length=50, blank=True, null=True)
    title = models.CharField(max_length=200, blank=True, null=True)
    description = RichTextField(blank=True, null=True)
    url = RichTextField(blank=True, null=True)
    skills = models.ManyToManyField(Skill, blank=True)
    is_active = models.BooleanField(default=True)
    order = models.IntegerField(default=1)

    def __str__(self):
        return self.title