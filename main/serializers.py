from rest_framework import serializers
from .models import (
		UserProfile,
		Experience,
		Portfolio,
		Testimonial,
		Category,
		Certificate,
		Interest,
		Media,
		ContactProfile,
    Skill
	)

class CategorySerializer(serializers.ModelSerializer):

  class Meta:
    model = Category
    fields = '__all__'

class SkillSerializer(serializers.ModelSerializer):
  category = CategorySerializer(read_only=True)
  class Meta:
    model = Skill
    fields = ('name','category','is_active','order')

class InterestSerializer(serializers.ModelSerializer):
  skills = SkillSerializer(many=True)
  class Meta:
    model = Interest    
    fields = ('title', 'body', 'date', 'url', 'skills', 'is_active', 'order', 'image')

class ExperienceSerializer(serializers.ModelSerializer):
  skills = SkillSerializer(many=True)
  class Meta:
    model = Experience    
    fields = ('timestamp', 'date', 'location', 'name', 'description', \
              'body', 'slug', 'is_active', 'skills', 'url', 'order')
    
class CertificateSerializer(serializers.ModelSerializer):
  skills = SkillSerializer(many=True)
  class Meta:
    model = Certificate    
    fields = ('date', 'organisation', 'title', 'description', 'url', \
              'skills', 'is_active', 'order' )
    
class UserProfileSerializer(serializers.ModelSerializer):
  skills = SkillSerializer(many=True)
  categories = SkillSerializer(many=True)
  class Meta:
    model = UserProfile    
    fields = ('user', 'avatar','avatar_personal', 'title', 'bio', 'bio_personal', \
              'skills', 'categories', 'cv', 'linkedin_url', 'github_url', 'gitlab_url')
    
class ContactProfileSerializer(serializers.ModelSerializer):

  class Meta:
    model = ContactProfile
    fields = ('timestamp', 'name','email', 'message')
    
class TestimonialSerializer(serializers.ModelSerializer):

  class Meta:
    model = Testimonial
    fields = '__all__'

class MediaSerializer(serializers.ModelSerializer):

  class Meta:
    model = Media
    fields = '__all__'

class PortfolioSerializer(serializers.ModelSerializer):

  class Meta:
    model = Portfolio
    fields = '__all__'
